#! /bin/bash
set -e
pwd
if [ -d dist ];
then
  echo "Sí, sí existe el dist."
  if [ -f dist/index.html ];
  then
    echo "Sí, sí existe el index."
  else
    echo "No, no existe el index."
    cd dist/index
  fi
else
  echo "No, no existe el dist."
  cd dist
fi

sleep 20