import Vuex from 'vuex'
import getTokenService from "../plugins/getToken";

const tokenData = process.env.tokenData 

const createStore = () => {
    return new Vuex.Store({
        state: {
            cargandocotizacion: false,
            config: {
                time: 0,
                aseguradora: '',
                cotizacion: false,
                emision: false,
                descuento: 0,
                telefonoAS: '',
                grupoCallback: '',
                from:'',
                habilitarBtnEmision: false,
                habilitarBtnInteraccion: false,
                idPagina:0,
                idCampana:0,
                idSubRamo:0,
                loading:true,
                urlRedireccion:'',
                msi:'',
                desc:'',
                accessToken:"",
                promo:"",
                dto: '',
				msi: '',
				promoLabel: '',
				promoSpecial: false,
				promoImg: '',
            },
            ejecutivo:{
                nombre:'',
                correo:'',
                id:0
            },
            formData: {
                idHubspot: '',
                aseguradora: '',
                marca: '',
                modelo: '',
                descripcion: '',
                detalle: '',
                clave: '',
                cp: '',
                nombre: '',
                telefono: '',
                gclid_field: '',
                correo: '',
                edad: '',
                fechaNacimiento: '',
                genero: '',
                emailValid: '',
                telefonoValid: '',
                codigoPostalValid: '',
                urlOrigen: ''
            },
            solicitud:{},
            cotizacion: {
                "Cliente": {
                    "FechaNacimiento": null,
                    "Genero": null,
                    "Direccion": {
                        "CodPostal": null,
                        "Calle": null,
                        "Colonia": null,
                        "Poblacion": null,
                        "Ciudad": null,
                        "Pais": null
                    },
                    "Edad": null,
                    "TipoPersona": null,
                    "Nombre": null,
                    "ApellidoPat": null,
                    "ApellidoMat": null,
                    "RFC": null,
                    "CURP": null,
                    "Telefono": null,
                    "Email": null
                },
                "Cotizacion": {
                    "PrimaTotal": 0
                },
                "Emision": {
                    "PrimaTotal": null,
                },
                "Vehiculo": {
                    "Descripcion": null,
                    "Marca": null,
                    "Modelo": null,
                },
                "Aseguradora": "-",
                "Coberturas": [
                    {
                        "DanosMateriales": "-",
                        "DanosMaterialesPP": "-",
                        "RoboTotal": "-",
                        "RCBienes": "-",
                        "RCPersonas": "-",
                        "RC": "-",
                        "RCFamiliar": "-",
                        "RCExtension": "-",
                        "RCExtranjero": "-",
                        "RCPExtra": "-",
                        "AsitenciaCompleta": "-",
                        "DefensaJuridica": "-",
                        "GastosMedicosOcupantes": "-",
                        "MuerteAccidental": "-",
                        "GastosMedicosEvento": "-",
                        "Cristales": "-",
                    }
                ],
            },
            servicios:{
                servicioDB:'http://138.197.128.236:8081/ws-autos/servicios'
            },
            conexCRM:
            {
                prospecto : {
                    numero: '',
                    correo: '',
                    nombre: '',
                    sexo: '',
                    edad: ''
                },
                productoSolicitud: {
                    idProspecto: '',
                    idTipoSubRamo: 1,
                    datos: ''
                },
                cotizaciones: {
                    idProducto: '',
                    idPagina: '',
                    idMedioDifusion: '',
                    idEstadoCotizacion: 1,
                    idTipoContacto: 1
                },
                cotizacionAli: {
                    idCotizacion: '',
                    idSubRamo: 1,
                    peticion: '',
                    respuesta: '',
                },
                solicitudes: {
                    idCotizacionAli: '',
                    idEmpleado: '',
                    idEstadoSolicitud: 1,
                    idEtiquetaSolicitud: 1,
                    idFlujoSolicitud: 1 ,
                    comentarios: ''
                },
            },
            cotizaciones: []
        },
        actions: {
            getToken(state) {
				return new Promise((resolve, reject) => {
					getTokenService.search(tokenData).then(
						resp => {
							if (typeof resp.data == 'undefined') {
								this.state.config.accessToken = resp.accessToken;
							} else if (typeof resp.data.accessToken != 'undefined') {
								this.state.config.accessToken = resp.data.accessToken;
							}
							localStorage.setItem('authToken', this.state.config.accessToken);
							resp = resp.data;
							resolve(resp);
						},
						error => {
							reject(error);
						}
					);
				});
			}
        },
        mutations: {
            validarTokenCore: function (state) {
                try {
                    if (process.browser) {
                        if (localStorage.getItem("authToken") === null || localStorage.getItem("authToken") === 'undefined') {
                            state.sin_token = true;
                            console.log('NO HAY TOKEN...')
                        } else {
                            console.log('VALIDANDO TOKEN...');
                            state.config.accessToken = localStorage.getItem("authToken");
                            var tokenSplit = state.config.accessToken.split('.');
                            var decodeBytes = atob(tokenSplit[1]);
                            var parsead = JSON.parse(decodeBytes);
                            var fechaUnix = parsead.exp
                            /*
                            * Fecha formateada de unix a fecha normal
                            * */
                            var expiracion = new Date(fechaUnix * 1000);
                            var hoy = new Date(); //fecha actual
                            /*
                            * Se obtiene el tiempo transcurrido en milisegundos
                            * */
                            var tiempoTranscurrido = expiracion - hoy;
                            /*
                            * Se obtienen las horas de diferencia a partir de la conversión de los
                            * milisegundos a horas.
                            * */
                            var horasDiferencia = Math.round(((tiempoTranscurrido / 3600000) + Number.EPSILON) * 100) / 100;

                            if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                                state.sin_token = 'expirado'
                            }
                        }
                    }
                } catch (error2) {
                    console.log(error2)
                }
            },
           
        }
    })
}
export default createStore
