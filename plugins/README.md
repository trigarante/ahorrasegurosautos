# PLUGINS

This directory contains your Javascript plugins that you want to run before instantiating the root vue.js application.

More information about the usage of this directory in the documentation:
https://nuxtjs.org/guide/plugins

**This directory is not required, you can delete it if you don't want to use it.**

//Flujo de trabajo
ahorrasegurosautos/pages/index.vue
    import headerComponent from '~/components/common/Header.vue'
    import formComponent from '~/components/forms/form.vue'
        import modelosService from "~/plugins/modelos";
            import autosService from './ws-autos'
        import descripcionesService from "~/plugins/descripciones";
            import autosService from './ws-autos'
        import subdescripcionesService from "~/plugins/subdescripciones";
            import autosService from './ws-autos'
        import detallesService from "~/plugins/detalles";
            import autosService from './ws-autos'
        import serviceCodigoPostal  from "~/plugins/codigoPostal";
            import baseWs from './ws-db'
        import avisoPrivacidadComponent  from "~/components/common/aviso-privacidad.vue";
        validateBeforeSubmit() -> path: "/thanks/" (pages/thanks.vue)
            layout: 'thanks', (layouts/thanks.vue)
                import FooterComponent from '~/components/common/Footer.vue'
            import headerwhiteComponent from '~/components/common/Header-white.vue'
    import spriteComponent from '~/components/common/sprite.vue'
    import elmejorComponent from '~/components/common/elmejorcomparador.vue'
    import contenidoComponent from '~/components/content/contenidoautos/contenido.vue'

nuxt.config.js
    '~plugins/bootstrap.js'
    '~/plugins/filters.js'

store/index.js
    import database from '~/plugins/saveData'
        import configDB from './configBase'
    import monitoreo from '~/plugins/monitoreo'
        import configDB from './configBase'
    import cotizacion from '~/plugins/cotizacion'
    import saveProspectoCRMService from '~/plugins/CRM'