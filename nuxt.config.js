module.exports = {
  target:'static',
  /*
  ** Headers of the page
  */ 
  head: {
    htmlAttrs: { lang: 'es-MX' },
    title: 'ahorraseguros',
    meta: [
      { charset: 'utf-8' },
      { hid: 'robots', name: 'robots', content: 'index, follow' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: 'ahorraseguros' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: 'favicon.png' }
    ]
  },

  loading: { color: '#3bd600' },
 
  build: {
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          //loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
 css: ['static/css/bootstrap.min.css','static/css/styles.css'],
 // include bootstrap js on startup
 plugins: [{ src: '~/plugins/filters.js', ssr: false },
 { src: '~/plugins/apm-rum.js', ssr: false }],
 
  env: {
    tokenData: 'mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=',
    // SERVICIOS CATALOGOS
    catalogo: "https://dev.ws-qualitas.com",
    /*PRODUCCION*/
    sitio: "https://p.ahorrasegurosauto.mx/",
    // coreAhorraSeguros: "https://core-ahorraseguros.com",
    /*PRUEBAS*/
    coreAhorraSeguros: "https://dev.core-ahorraseguros.com",
    /**LOCAL*/
    //newCoreAhorraSeguros: 'http://localhost:5000',
   /**VALIDACIONES */
   urlValidaciones: 'https://core-blacklist-service.com/rest',
    promoCore: "https://dev.core-persistance-service.com",
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    
    Environment:'DEVELOP',
  },
  render: {
    http2:{ push: true },
    resourceHints:false,
    compressor:{ threshold: 9 }
  }
}